
;###########################################################################################
;############# Variables ###################################################################
;###########################################################################################

extensions [
           ; r
           ]


globals [
          THE_SEED
          NUMBER_OF_CANDIDATES
          CROSSOVER-RATE
          MUTATION-RATE
          ;         GA-ROUNDS          ; exogenised
          WORKHOURS
          
          WORLD_MAX_DISTANCE           ; the maximally possible distance between two agens in any given size of world 
          MEMORY_LENGTH
          ;LOYALTY_THRESHOLD           ; wont need this, thereshold is disabled atm
          
          GPROB

;Round Statistics 
        SpecialisationVector           ;Share of an agent's Prod_Settingsuction 
        ;EfficiencyVector
        LeisureVector 
        DegreeVector
        SearchesVector
        ExchangeVolumeVector
        
         ]


turtles-own [
             Leisure                    ;[0 10]
             Autokrat_Leisure           ;?
             Demand                     ; can Include "0"s
             Autokrat_Cost              ; list of isolated Prod_Settingsuction costs
             
             Prod_Settingsuction_Exponents       ; curvature, currently initiated as [1] [1 2]
             Prod_Settingsuction_Coefficients    ; slope, initiated as [random-normal 1 0.3]

             Contracts_Held             ; list of contracts that are up for negotiation
             Contracts_Committed        ; list of contracts that need to be fulfilled this round and cannot be negotiated again
             Contracts_Sent             ; list of contracts previously held, but set out to others for fullfillment

             Preference_for_Loyalty     ; specifies to which degree the agent prefers to find partners based on prior memory as opposed to mere distance
             Neighbour_List             ; static list of all the neighbours (for order)
             Neighbour_Distance_Scores  ; static? list of scores regarding the distance of neighbours
             Neighbour_Memory_Scores    ; dynamic list representing the latest memories regarding the outcomes of exchanges between agents
             
             Round_Partner              ; this will be a temporary list that contains the partners that the agent has committed and exchange to
             Round_Out                  ; this list will contain records of the exchanges the agent made in every deal in that round
             Round_In                   ; this is a list with all the orders received
             
             Prod_Settingsuction_Memory          ; list of time spent Prod_Settingsucing units of goods
             ;Best_Experience            ; Remembers the agent's best experience to date - currently we dont need this
             Available_for_Exchange     ; TRUE FALSE - indicates if the agent is still looking for an echange partner in this round
             
             
             Leisure_Memory             ; list of length Mem_Length storing the agent's most recently achieved levels of leisure
             Search_Prob_Vector         ; Vector of Probabilities to undertake n searches for partners, innitiated as geometric distribution with parameter GPROB
             Round_Searches             ; every round, the agent will update this variable by choosing a number of searches from the 'Search_Prob_Vector'
             Round_Searches_left        ; indicates the number of searches still available to the agent in any given round
             ]

Links-own [
           
           ] 


;###########################################################################################
;############# Analysis Procedures #########################################################
;################### tick ##################################################################
;###########################################################################################

; Degree of Specialisation (in the right direction)
to-report DegreeOfSpecialisation [CHeld CCommitted]
  let tmpSpec 0
  let tmp_Prod_Settings Extract-Prod_Settingsuction-From-Contracts (sentence CHeld CCommitted)
  
  if Prod_Settings = "CSW"
         [ ifelse who mod 2 = 0
                 [ ;even Agents - supposed to Prod_Settingsuce 0
                   set tmpSpec item 0 tmp_Prod_Settings / sum tmp_Prod_Settings
                   ]
                 [ ;odd Agents - supposed to Prod_Settingsuce 1
                   set tmpSpec item 1 tmp_Prod_Settings / sum tmp_Prod_Settings
                 ]
         ]
  if Prod_Settings = "homoP" 
       [
         ifelse who mod 2 = 0
                 [ ;even Agents - supposed to Prod_Settingsuce 0 and 1
                   set tmpSpec ( item 0 tmp_Prod_Settings + item 1 tmp_Prod_Settings ) / sum tmp_Prod_Settings
                   ]
                 [ ;odd Agents - supposed to Prod_Settingsuce 1
                   set tmpSpec ( item 2 tmp_Prod_Settings + item 3 tmp_Prod_Settings ) / sum tmp_Prod_Settings
                 ]
       ]
if Prod_Settings = "heteroP"
       [
                
           if who = 0 or who = 1 or who = 2
             [ ;supposed to Prod_Settingsuce 0
               set tmpSpec (item 0 tmp_Prod_Settings)  / sum tmp_Prod_Settings
              ]
           if who = 3 or who = 4 or who = 5
             [ ;supposed to Prod_Settingsuce 1
               set tmpSpec (item 1 tmp_Prod_Settings)  / sum tmp_Prod_Settings
              ]
           if who = 6 or who = 7 or who = 8
             [ ;supposed to Prod_Settingsuce 2
               set tmpSpec (item 2 tmp_Prod_Settings)  / sum tmp_Prod_Settings
              ]
           if who = 9 or who = 10 or who = 11
             [ ;supposed to Prod_Settingsuce 3
               set tmpSpec (item 3 tmp_Prod_Settings)  / sum tmp_Prod_Settings
              ]
         ]
if Prod_Settings = "CSW"[]
               
       
       
  report tmpSpec
end


;; A priory efficiency - Leisure as percentage of specialist/competitive lesure
;to-report PercentageOfIdealProd_Settings [numLeisure]
;  let tmpIdeal 0
;                ifelse who mod 2 = 0
;                               ; (L.achieved - L.ideal )        /  (L.ideal)
;                 [ ;even Agents - supposed to Prod_Settingsuce 0
;                   set tmpIdeal (numLeisure - (10 - 6.430931) ) / (10 - 6.430931)
;                   ]
;                 [ ;odd Agents - supposed to Prod_Settingsuce 1
;                   set tmpIdeal (numLeisure - (10 - 5.720776) ) / (10 - 5.720776)
;                 ]
;  report tmpIdeal
;end

;RelevantDegree Neighbour_Memory_Scores - relations that 
to-report RelevantDegree [MemScores]
  let MScores map sum MemScores
  let MPercentages []
  ifelse (sum MScores) = 0
           [set MPercentages []]
           [set MPercentages map  [ ? / (sum MScores) ] MScores]
  let Degree length filter [ ? > 0.1 ] MPercentages
  report Degree
end



to PrepareStats
  
  set SpecialisationVector []
  set LeisureVector        [] 
  set DegreeVector         []
  set SearchesVector       []
  set ExchangeVolumeVector []
  
  foreach sort turtles [ask ? [
      set SpecialisationVector lput (DegreeOfSpecialisation Contracts_Held Contracts_Committed) SpecialisationVector
     ; set EfficiencyVector lput (PercentageOfIdealProd_Settings Leisure) EfficiencyVector
      set LeisureVector lput  Leisure LeisureVector
      set DegreeVector lput (RelevantDegree Neighbour_Memory_Scores)   DegreeVector
      set SearchesVector lput Round_Searches SearchesVector
      set ExchangeVolumeVector lput (length Contracts_Committed  + length  Contracts_Sent) ExchangeVolumeVector
     ]
  ]
end
                               
    
 
 
 
;###########################################################################################
;############# Main Procedures #############################################################
;################## SETUP ##################################################################
;###########################################################################################



to setup
  clear-all 
  set THE_SEED random 10000 * random 10000 ;change this to reporter new-seed
  random-seed THE_SEED
  ; GA related Parameters
  set NUMBER_OF_CANDIDATES 30                   ; number of candidate bit strings for each GA  
  set CROSSOVER-RATE 70                         ; this many percent of candidate bits will be crossed in any GA's next generation
  set MUTATION-RATE 1                           ; percentage of mutations for each of the bits in any GA's next Generation
  ;set GA-ROUNDS max (list (2 * No_Goods) 20)    ; how often will the GA be run to find a decent solution

  set WORLD_MAX_DISTANCE sqrt ((max-pxcor ^ 2)  + (max-pycor ^ 2)) ; max distance between agents in any symmetric world
  
  set MEMORY_LENGTH Mem_Length  ; length of memory regarding the number of past exchanges with other agents
  ;set LOYALTY_THRESHOLD (0.1 * MEMORY_LENGTH)      ; sets the minimum value for remembered time savings after which agents start to scale up their experience scores and let if affect their partner choice subnstantially.
  set WORKHOURS  10; No_Goods        ; so that the time savings between runs remain comparable
  set GPROB 0.7                 ;Parameter to control the Geometric Distribution that models the search number probabilities
  crt NO_AGENTS [
                setxy random-xcor random-ycor                             ; distribute agents evenly at random 
                ]
   ; CSW Prod_Settingsuction and Demand
if Prod_Settings = "4G" 
       [
         if No_Goods != 4 or No_Agents != 12 [user-message "The settings are not consistent with EGS 4G"]  
         ask Turtles [
               set Demand [ 6 6 6 6 ]  
           
           ifelse who mod 2 = 0
             [ ;even Agents
               set Prod_Settingsuction_Exponents    [2.5 2 1 1]  
               set Prod_Settingsuction_Coefficients [1 1 2 2.4] 
             ]
             [ ;odd Agents
               set Prod_Settingsuction_Exponents     [1 1 2 2.5 ] 
               set Prod_Settingsuction_Coefficients  [2.4 2 1 0.5] 
             ]
         ]          
         ]

if Prod_Settings = "4Gh"
       [
         if No_Goods != 4 or No_Agents != 12 [user-message "The settings are not consistent with EGS 4Gh"]   
         ask Turtles [
           set Demand [ 6 6 6 6 ]  
           
           if who = 0 or who = 1 or who = 2
             [ ;even Agents
               set Prod_Settingsuction_Exponents    [2.5 1 1 1]  
               set Prod_Settingsuction_Coefficients [1 2 2.4 2.4] 
              ]
           if who = 3 or who = 4 or who = 5
             [ ;even Agents
               set Prod_Settingsuction_Exponents    [ 1   2.5 1 1]  
               set Prod_Settingsuction_Coefficients [ 2.4 1   2 2.4 ] 
              ] 
           if who = 6 or who = 7 or who = 8
             [ ;even Agents
               set Prod_Settingsuction_Exponents    [ 1   1   2.5 1]  
               set Prod_Settingsuction_Coefficients [ 2.4 2.4 1   2 ] 
              ] 
           if who = 9 or who = 10 or who = 11
             [ ;even Agents
               set Prod_Settingsuction_Exponents    [ 1 1   1   2.5]  
               set Prod_Settingsuction_Coefficients [ 2 2.4 2.4 1 ] 
              ] 
 
 
           ]          
         ]
 if Prod_Settings = "CSW" [
         if No_Goods != 2 [user-message "The settings are not consistent with EGS 4G"]  
         ask Turtles [
              ifelse who mod 2 = 0
                 [ ;even Agents
                   set Prod_Settingsuction_Exponents [2 1]  
                   set Prod_Settingsuction_Coefficients [1.1 2.53] 
                   set Demand [26 13]   
                   ]
                 [ ;odd Agents
                   set Prod_Settingsuction_Exponents [1 2.5] 
                   set Prod_Settingsuction_Coefficients  [2.25 0.41] 
                   set Demand  [10 30]
                 ]
               ]
 ]
  if Prod_Settings = "none" [user-message "Please specify demand and production and adjust the specialisation monitor"]  


 
  ask Turtles [
                set Contracts_Held Initiate_Contracts Demand              ; Prod_Settingsuces a list of "contract" tripplets according to the agent's demand
                set Contracts_Committed []                                ; all incomming contracts, (and the own ones with the respective goods) will be transferred here - committed contracts are not negotiable anymore
                set Contracts_Sent []                                     ; upon outsourcing a contract, it will be added to this list          

                set Autokrat_Cost Calculate-Prod_Settingsuction-Time Contracts_Committed Contracts_Held Prod_Settingsuction_Coefficients Prod_Settingsuction_Exponents; remembers ho long it takes to Prod_Settingsuce each of the goods individually
                set Leisure Calculate_Leisure Contracts_Held Contracts_Committed Prod_Settingsuction_Coefficients Prod_Settingsuction_Exponents ; workinghours -  how much time is spent on Prod_Settingsuction in autarky
                set Autokrat_Leisure Leisure ; just to remember 
                set Leisure_Memory n-values MEMORY_LENGTH [Autokrat_Leisure]  ; Leisure_Memory is innitiated as the autarchy level of leisure throughout.
                ;agents calculate their time per unit spend Prod_Settingsucing 1 unit for all goods and also, should they require more than 1 unit in autarky, 
                ; the time spend Prod_Settingsucing these amounts of goods
                set Prod_Settingsuction_Memory Initiate_Prod_Settingsuction_Memories Contracts_Held Prod_Settingsuction_Coefficients Prod_Settingsuction_Exponents 
                set Search_Prob_Vector Initiate_Search_Prob_Vector GPROB
                ]

  
  
  ask Turtles [
               set Preference_for_Loyalty Loyalty / 100                                               ; so far a fixed value - 50% - indicating how strongly the agents choose partners based on prior experiences 
               set Neighbour_List sort other turtles                                                  ; everyone prepare a list of all other turtles ordered by "who"
               set Neighbour_Distance_Scores Calculate_Distance_Scores Neighbour_List                 ; closest will be 100, then its 100/"ceiling of actual distance"
               set Neighbour_Memory_Scores n-values length Neighbour_List [n-values MEMORY_LENGTH [0]] ; initiate as a list of lists of MEMORY_LENGTH zeroes
              ]
  PrepareStats
  reset-ticks 
end






;###########################################################################################
;############# Main Procedures #############################################################
;################### GO ####################################################################
;###########################################################################################


to go
  ; at the beginning of each round, the agents erase all temporary memory they had from the previous round:
  ask turtles [
                set Contracts_Held Initiate_Contracts Demand              ; Prod_Settingsuces a list of "contract" tripplets according to the agent's demand
                set Contracts_Committed []                                ; all incomming contracts, (and the own ones with the respective goods) will be transferred here - committed contracts are not negotiable anymore
                set Contracts_Sent []                                     ; upon outsourcing a contract, it will be added to this list 
                
                set Round_Partner []                                      ; ordered list of all the partners interacted with in the coming round
                set Round_In []                                           ; contracts received in this round, same order as Round_Partner
                set Round_Out []                                          ; contracts sent out in this round, dto.
                
                set Available_for_Exchange TRUE
                set Round_Searches Decide_NSearches Search_Prob_Vector    ; decide how much time to spend on Searches, depending on the Probability Vector "Search_Prob_Vector"
                set Round_Searches_left Round_Searches
               ]

;%%%%%% Main Workhorse - Negotiations %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
; all agents get a max of 3 attempts per round at initiating an exchange
; when all these attempts are used up and/or there are no agents interested in an exchange, the search part of the round ends
  while [any? turtles with [Round_Searches_left > 0 AND Available_for_Exchange]]  
            [
             ask one-of turtles with [Round_Searches_left > 0 AND Available_for_Exchange]
                  [
                    Negotiate-Exchange
                   ]
             ]        

;Prod_Settingsuction - i.e. calculate working hours and spare time; learn and update memory

;LEARN to Prod_Settingsuce
ask turtles [
             set Prod_Settingsuction_Memory Update_Prod_Settingsuction_Memories Prod_Settingsuction_Memory Contracts_Committed Contracts_Held Prod_Settingsuction_Coefficients Prod_Settingsuction_Exponents
            ]


;UPDATE MEMORY
ask turtles [
              let Prod_Settingsuction_Time Calculate-Prod_Settingsuction-Time Contracts_Committed Contracts_Held Prod_Settingsuction_Coefficients Prod_Settingsuction_Exponents 
              let Prod_Settingsuction_Units Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts_Held
              
              set Leisure Workhours - (sum Prod_Settingsuction_Time) - (Search_Cost * Round_Searches)
              set Search_Prob_Vector Update_Search_Prob_Vector Search_Prob_Vector Leisure Leisure_Memory Round_Searches
              set Leisure_Memory Update_Leisure_Memory Leisure_Memory Leisure
              
              set size Leisure
                  ifelse Leisure <= 0 [set shape "x"][set shape "default"]
              
              ; 1) see if there was more than one exchange with the same partner
              
              let tmp_CRR Consolidate_Round_Results Round_Partner Round_Out Round_In
              ; CRR look like this ; partners: [ [(turtle 3)                                             (turtle 6)]   
                                   ; out     :   [[1 1 (turtle 1)] [0 1 (turtle 1)] [4 1 (turtle 1)]]    [[3 1 (turtle 1)]] ]  
                                   ; in      :   [[2 1 (turtle 3)] [3 1 (turtle 3)]]                     [[2 1 (turtle 6)]] ]  
              ;                                 ]
              ; 2) calculate how much time was spent performing the tasks that were committed for each of the partners
              
              
              
              let Units_Prod_Settingsuced_per_Partner  []
              foreach (item 2 tmp_CRR) [set Units_Prod_Settingsuced_per_Partner lput (Calculate-Prod_Settingsuction-For-Agent [] ?) Units_Prod_Settingsuced_per_Partner ]
;                 let Partner_Costs []
;                    foreach Units_Prod_Settingsuced_per_Partner  [ set Partner_Costs lput (map [ ifelse-value (?2 != 0) [?1 * ?3 / ?2][0] ] Prod_Settingsuction_Time Prod_Settingsuction_Units ?) Partner_Costs ]
;                 print Partner_Costs
              ; 3) estimate savings
              let Units_Saved_Per_Partner []
              foreach (item 1 tmp_CRR) [set Units_Saved_Per_Partner lput (Calculate-Prod_Settingsuction-For-Agent [] ?) Units_Saved_Per_Partner ]
              
              let Prod_Settingsuction_WO_Partner []
              let tmp 0
              ; calculate for each exchange partner the vector of units Prod_Settingsuced
              foreach (item 0 tmp_CRR) [
                                        set Prod_Settingsuction_WO_Partner lput (map [ ?1 - ?2 + ?3  ] Prod_Settingsuction_Units (item tmp Units_Prod_Settingsuced_per_Partner) (item tmp Units_Saved_Per_Partner) ) Prod_Settingsuction_WO_Partner 
                                        set tmp tmp + 1 
                                         ]
              let Savings_Estimates []
              
              ;estimate how much time was saved due to the exchanges with any given partner - *Savings_Estimates*
                                                                           ; estimates the time needed for Prod_Settingsuction  w/o that partner  ; sum of actual Prod_Settingsuction time
              foreach Prod_Settingsuction_WO_Partner [set  Savings_Estimates lput ((Estimate_Prod_Settingsuction_Time_Set ? Prod_Settingsuction_Memory) -          (sum Prod_Settingsuction_Time)) Savings_Estimates]
             
              ; update memory about the agents' expectations about each other
              ; 1) a neighbour's position is identified in the neighbour list
              ; 2) accordingly the memory entry at that position is updated:
              ; 3) the new experience becomes first in the list of memories, and the last entry is dropped (i.e. length stays the same)
              ; if there is no new experience for that particular agent in this round, the agent forgets a but of information, adding a "0" to the list
              foreach Neighbour_List [
                                       let pos_mem position ? Neighbour_List       ; find that agent in my list of neighbours
                                       let pos_outcome position ? (item 0 tmp_CRR) ; see if there were exchanges with him this round
                                       ifelse is-number? pos_outcome               ; if there were exchanges, add the estimate of the time saved to the memory of that agent
                                         [set Neighbour_Memory_Scores replace-item pos_mem Neighbour_Memory_Scores (fput (item pos_outcome Savings_Estimates) but-last (item pos_mem Neighbour_Memory_Scores) )]
                                                                                   ;if there was no exchange, the agent remembers "0" for this round
                                         [set Neighbour_Memory_Scores replace-item pos_mem Neighbour_Memory_Scores (fput 0                                    but-last item pos_mem Neighbour_Memory_Scores )]
                                         
                                        ]
             ]
tick

PrepareStats
;print (word "______________________")
;print (word "SpecialisationVector: " SpecialisationVector)
;print (word "EfficiencyVector: " EfficiencyVector)
;print (word "LeisureVector: " LeisureVector)
;print (word "DegreeVector: " DegreeVector )
;print (word "SearchesVector: " SearchesVector)
;print (word "ExchangeVolumeVector: " ExchangeVolumeVector)

end


;###########################################################################################
;############# Main Procedures #############################################################
;################ END of GO ################################################################
;############## Negotiations ###############################################################
;###########################################################################################




; This agent procedure wraps the entire negotiation between two agents:
; an agent chooses a partner, according to its distance and experiences
; then tey try to negotiate an agreement using the GA Negotiate-Task-Allocation-For 
; and lastly, once they have found and agreement both agents update their contracts_held (deleting the contracts exchanged), 
; the contracts committed (adding those taken over, and their own for the same goods) and the contracts_sent (adding those that the other agent is taking over)(checked)
to Negotiate-Exchange
 let Partner Select_Partner Preference_for_Loyalty Neighbour_List Neighbour_Distance_Scores Neighbour_Memory_Scores
 set Round_Searches_left Round_Searches_left - 1 ; one attempt in this round used

 ifelse Partner = nobody
    [
    ]
    [   
             ; part on reporting the initial state     
             let my_orig_time_estimate Estimate_Prod_Settingsuction_Time_Set (Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts_Held) Prod_Settingsuction_Memory
             let partners_orig_time_estimate 0
                ask Partner [set partners_orig_time_estimate Estimate_Prod_Settingsuction_Time_Set (Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts_Held) Prod_Settingsuction_Memory]
                                 
         let Agreement Negotiate-Task-Allocation-For  [Contracts_Held] of self [Contracts_Held] of Partner self Partner
         ;print Agreement ;(list very_final_Allocation (item pos_final_Allocation tmp_Fitness) Contracts-ForA Contracts-ForB) ; maybe include expected time savings as well                              
                         ;Contracts-BToA   ;Contracts-AToB)
         Update-Contracts item 2 Agreement item 3 Agreement
         ask Partner [Update-Contracts item 3 Agreement item 2 Agreement]
              
              ;part on reporting the state after the fact                  
              let my_updated_time_estimate Estimate_Prod_Settingsuction_Time_Set (Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts_Held) Prod_Settingsuction_Memory
              let partners_updated_time_estimate 0
                  ask Partner [set partners_updated_time_estimate Estimate_Prod_Settingsuction_Time_Set (Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts_Held) Prod_Settingsuction_Memory]
        
        ;wrap-up, adjust the signal if agents dont have anything to trade after the exchange
        if empty? Contracts_Held [set Available_for_Exchange FALSE]
        ask Partner [if empty? Contracts_Held [set Available_for_Exchange FALSE]]

        ; remember partners and expected savings
        set Round_Partner lput Partner Round_Partner 
        set Round_In lput item 2 Agreement Round_In
        set Round_Out lput  item 3 Agreement Round_Out
        ask Partner [
                     set Round_Partner lput myself Round_Partner 
                     set Round_In lput item 3 Agreement Round_In
                     set Round_Out lput item 2 Agreement Round_Out
                    ]
    ] ; close ifelse partner != nobody
end



;Agent procedure that updates the Agent's internal variables after an exchange agreement has been reached with another agent (checked)
to Update-Contracts [TasksReceived TasksSent]
       ; Tasks received will be committed: i.e. they are not up for negotiation with other agents in this round. 
       ; this setting should be investigated in future analyses. agents could learn and become brokers etc.
       set Contracts_Committed sentence  Contracts_Committed TasksReceived
       
       ; in order to benefit from economies of scale, agents will commit to their own tasks for a certain good once they take over the Prod_Settingsuction for another agent
       ; this has several reasons: A) it reflects the intentional pursuit of economies of scale
       ; B) otherwise, the agents would go on exchanging tasks until the have gotten rid of all their tasks and Prod_Settingsuce only for others
       ; c) trail runs showed that without this precaution the negotiation rounds may only finish when the GA eventually Prod_Settingsuced an outcome that for both parties 
       ; leads to a loss but renders them both unavailable for exchange
;##################################################################
;              foreach TasksReceived [
;                              let tmp_Good item 0 ?
;                              while [member? (list tmp_Good 1 self) Contracts_Held]
;                                           [
;                                             let tmp_pos position (list tmp_Good 1 self) Contracts_Held
;                                             set Contracts_Held remove-item tmp_pos Contracts_Held
;                                             set Contracts_Committed lput (list tmp_Good 1 self) Contracts_Committed                                         
;                                             ]
;                              ]
       ; remember which tasks have been given away to other agents (so far does not remember to whom - maybe that information becomes relevant at some stage)       
       set Contracts_Sent sentence Contracts_Sent TasksSent 
       ; those tasks that are sent to other agents will be removed from the list of contracts up for negotiation
       foreach TasksSent [
                          let pos position ? Contracts_Held
                          set Contracts_Held remove-item pos Contracts_Held
                         ]
        
end


; this reporter is made to combines all the exchanged contacts in any given round, that come form one agent -i.e. potentially combining several transaction into one item.
; it then returns three lists: the partners, the tasks tkey received and the tasks the agent took over in return
; these lists will the allow to calculate the costs and estimate the benefits for each partner (checked)
to-report Consolidate_Round_Results [Partner Out In]
  let CPartner remove-duplicates Partner
  let COut n-values length CPartner [[]]
  let CIn n-values length CPartner [[]]
  ifelse length CPartner < length Partner
       [
         let tmp 0
         foreach Partner [
                          let tmp_repl position ? CPartner
                          set COut replace-item tmp_repl COut (sentence (item tmp_repl COut) item tmp Out)
                          set CIn  replace-item tmp_repl CIn  (sentence (item tmp_repl CIn)  item tmp In)
                          set tmp tmp + 1
                           ]
       ]
       ; if there is nothing to change, it just reports back the values
       [
         set COut Out
         set CIn In
         ]
  report (list CPartner COut CIn)
end














;turtle reporter, selects a partner for exchange, based on the agent's preference for exploration or exploitation, the agent's prior experiences (time savings) and 
; the distances between agents
to-report Select_Partner  [prefLoyalty Neighbours Distances Experiences]
  
  let memory_scores []
  foreach Experiences [set memory_scores lput (sum ?) memory_scores]  ;memory scores are the sums of experiences
  
                                     ; keep the record of best past experience constantly updated.
                                     ;if max Memory_Scores > Best_Experience [set Best_Experience max Memory_Scores] ; the max of these sums of experiences is compared to the threshold
                                     ; effect of experience kicks in only after the best experience crosses a certain threshold - i.e. agents are a little cautious and want to sample first, then commit
                                     ;if  Best_Experience > LOYALTY_THRESHOLD
                                     ;   [ set Memory_Scores map [ 50 * ? / Best_Experience] Memory_Scores ] 
  
  if sum memory_scores > 0 ; there must be some positive experience at least - only then will the scoring be computed (max needs to be strict positive, or the division will lead to strange results
  [ set memory_scores map [ (50 * ?) / (max memory_scores)] memory_scores ] 

  
  let NeighbourScores (map [prefLoyalty * ?1 + (1 - prefLoyalty ) * ?2] Memory_Scores Distances)
  set NeighbourScores (map [ifelse-value (? < 0) [0][?] ] NeighbourScores )
  
  let tmp_Partner Pick_a_Partner Neighbours NeighbourScores
  if Search_Random [set tmp_Partner one-of other turtles]
  let Partner tmp_Partner
  if not [Available_for_Exchange] of Partner [set Partner nobody]
  
  
;  let Partner nobody
;  let attempts 0
;  let Partner_NA Partner_Refuses self tmp_Partner
;  while [Partner_NA AND (attempts <= 10)]  ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% attempts must go. only one attempt per search!
;           [
;             set tmp_Partner Pick_a_Partner Neighbours NeighbourScores
;             if Search_Random [set tmp_Partner one-of other turtles]
;             set Partner_NA Partner_Refuses self tmp_Partner
;             set attempts attempts + 1
;           ]
;  if not (Partner_NA) [set Partner tmp_Partner]

  report Partner
end  


;This little reporter determines a chosen partner's agreement to start negotiations.
; depending on whether Rejection is possible or not, the selected partners are more or less critical.
; if Rejection is off, they will agree to every exchange that is proposed to them.
; if Rejection is on, they will reject every agent that scores below 0 in the sum of their memories of exchanges with this agent
; also if Rejection is on, and the proposing agent scores only 0 (i.e. no remembered interactions) there is a 50/50 change of rejection
; the reporter reports FALSE when the rejection is negated, i.e. when the partner agrees to negotiate
; it returns TRUE when the partner rejects the offer

to-report Partner_Refuses [Agent Partner]
  let answer FALSE
  ask Partner [
              if not Available_for_Exchange [set answer TRUE]
;              if Rejection [   
;                            let posP position Agent Neighbour_List
;                            let Score item posP (map [sum ?] Neighbour_Memory_Scores)
;                            if Score < 0 [set answer TRUE]
;                            let tmp random-float 1 
;                            if Score = 0 and tmp < 0.5 [set answer TRUE]
;                           ]     
                 ]
  report answer
end

; sub-procedure for Select_Partner; does the actual selection part, relative to the agent's scores derived from distances and memories (checked)
to-report Pick_a_Partner [Neighbours Scores]

let Target_Score random-float (sum Scores)
  let tmp_Score 0
  let tmp 0
  let tmp_Partner nobody
  ifelse Target_Score > 0
      [        
       while [tmp_Score <= Target_Score ]
            [
             set  tmp_Score tmp_Score + (item tmp Scores)
             set  tmp tmp + 1
            ]
       set tmp_Partner item (tmp - 1) Neighbours
      ]
      [
        set tmp_Partner item 0 Neighbours
        ]

  report tmp_Partner
end



;###########################################################################################
;############# Setup Procedures ############################################################
;###########################################################################################


; turtle reporter procedure - uses "self"
; reporter that translates list Demand into individual "Orders" containing the Good's index, the amount demanded and the ordering agent
;to-report Initiate_Contracts [MyDemand]
 ; let tmpContracts []
  ;let tmp 0
  ;foreach MyDemand [
   ;                 if ? > 0 [set tmpContracts lput (list tmp ? self) tmpContracts]
    ;                set tmp tmp + 1
     ;               ]
  ;report tmpContracts 
;end


; turtle reporter procedure - uses "self"
; reporter that translates list Demand into individual "Orders" containing the Good's index, the amount demanded and the ordering agent
to-report Initiate_Contracts [MyDemand]
  let tmp_demand MyDemand
  let tmpContracts []
  let tmp 0
  foreach tmp_demand [
                      repeat ? [set tmpContracts lput (list tmp 1 self) tmpContracts]
                      set tmp tmp + 1
                     ]
  report tmpContracts 
end




; agent reporter, returns the standardized distance score between the caller and any other agent in the game
; the standardised distance is always between 50 and 0. A high distance score indicates a short distance between agents.
to-report Calculate_Distance_Scores [AgentList]
  let tmp_Scores []
      foreach AgentList [ set tmp_Scores lput  (50 - (50 * distance ? / WORLD_MAX_DISTANCE)) tmp_Scores ]
  report tmp_Scores
end


;###########################################################################################
;############# GO  Procedures ##############################################################
;###########################################################################################

; observer reporter that uses a GA to derive a task allocation between two agents that is of high fitness, regarding 
; a fitness function (including time savings, fairness and non-loss of time)
to-report Negotiate-Task-Allocation-For [TasksFromA TasksFromB AgentA AgentB]
  ; combine the agents' negotiable contracts
  let Collection-Of-Contracts sentence TasksFromA TasksFromB
  let tmp_Allocation Create-Candidates (NUMBER_OF_CANDIDATES - 1) (length Collection-Of-Contracts)
 
  ; tweak of the GA It will always be initiated with a candidate where the agents change nothing - and additionally one where they change everything
  let NOChangesCandidate sentence (n-values (length TasksFromA) [0]) (n-values (length TasksFromB) [1])
  let ALLChangesCandidate sentence (n-values (length TasksFromA) [1]) (n-values (length TasksFromB) [0])
  
  set tmp_Allocation lput NOChangesCandidate tmp_Allocation 
  set tmp_Allocation lput ALLChangesCandidate tmp_Allocation 
  
  let tmp_Fitness Calculate-Fitness-All-Candidates Collection-Of-Contracts tmp_Allocation AgentA AgentB

  repeat GA-ROUNDS [
                    set tmp_Allocation Create-Next-Generation-Candidates tmp_Allocation tmp_Fitness
                    set tmp_Fitness    Calculate-Fitness-All-Candidates Collection-Of-Contracts tmp_Allocation AgentA AgentB  
                   ]



  let pos_final_Allocation (position (max tmp_Fitness) tmp_Fitness)
  let final_Allocation item pos_final_Allocation tmp_Allocation
  ; tweak - agents can always back out if their negotiation provides no improvement
  if   (max tmp_Fitness) < 0 [set final_Allocation NoChangesCandidate]
  
; here we could include another safety mechanism, allowing the agents to refuse any allocation with negative expectations
  
  ;the GA makes no distinction regarding who brought in which task, this function reverses unnecessary swaps,
  ; so that identical tasks on both sides, stay where they originated from
  let very_final_Allocation Re-allocate-Tasks Final_Allocation Collection-Of-Contracts TasksFromA TasksFromB


  let Contracts-BToA [] ; the tasks that A will take over from B
  let Contracts-AToB [] ; the tasks that B will take over from A
 
  let tmp 0
  foreach very_final_Allocation [
                                 if ? = 1 AND tmp <  (length TasksFromA) [ set Contracts-AToB lput (item tmp Collection-Of-Contracts) Contracts-AToB ]
                                 if ? = 0 AND tmp >= (length TasksFromA) [ set Contracts-BToA lput (item tmp Collection-Of-Contracts) Contracts-BToA ]
                                 set tmp tmp + 1 
                               ] 
;A is turtle 7 with 4 tasks, B is 4 with 4 tasks  
;[[1 0 0 0 1 1 0 1]    [[0 1 (turtle 7)] [1 1 (turtle 7)] [2 1 (turtle 7)] [3 1 (turtle 7)]      [0 1 (turtle 4)] [1 1 (turtle 4)] [2 1 (turtle 4)] [3 1 (turtle 4)]]  [[2 1 (turtle 4)]]   [[0 1 (turtle 7)]]    ]


report (list very_final_Allocation Collection-Of-Contracts  Contracts-BToA Contracts-AToB)
end 

; this observer reporter reverses unneccessary exchanges of tasks as a result from the allocation GA.
; the GA does not factor in which task originates from which agent, so it is possible that agentA ends up fulfilling 
; say (1 1 agentB) while agentB does (1 1 agentA). the GA does not see these swaps as different, so this 
; procedure reverses swaps like this one.
; inputs: final allocations of tasks after GA, innitial tasks to be negotiated
; remember: 0 means that agentA takes over a task, so the original allocation is like [ 0 0 0 0 0 1 1 1 1 1 ] [with length(TasksA) 0s]
; checked
to-report Re-allocate-Tasks [Allocation Collection-Of-Contracts TasksA TasksB]
  let LimitAB (length TasksA)
  let Exchanged n-values length Allocation [FALSE]
      let tmp 0 
      foreach Allocation [
                          if ? = 1 and tmp <  LimitAB [ set Exchanged replace-item tmp Exchanged TRUE]
                          if ? = 0 and tmp >= LimitAB [ set Exchanged replace-item tmp Exchanged TRUE]
                          set tmp tmp + 1
                         ]
      ;print Exchanged
   
  let tmpTA TasksA
  let tmpTB TasksB
  let fin_Allocation Allocation
  let tmp1 0
    while [tmp1 < length TasksA] [
                                  let tmp2 0
                                  while [tmp2 < length TasksB] [
                                                              if Are-Tasks-Paired? (item tmp1 tmpTA) (item tmp2 tmpTB)
                                                                [ 
                                                                 if (item tmp1 Exchanged) AND (item (limitAB + tmp2) Exchanged)
                                                                   [
                                                                    ;set Pairs replace-item tmp1 Pairs tmp2
                                                                    ;set Pairs replace-item (limitAB + tmp2) Pairs tmp1
                                                                    ; changing these two back to normal (0s first, then 1s)
                                                                     set fin_Allocation replace-item tmp1 fin_Allocation 0
                                                                     set fin_Allocation replace-item (limitAB + tmp2) fin_Allocation 1
                                                                     set tmpTA replace-item tmp1 tmpTA "gone"
                                                                     set tmpTB replace-item tmp2 tmpTB "gone2"
                                                                   ]
                                                                ]
                                                             set tmp2 tmp2 + 1
                                                            ] 
                                 set tmp1 tmp1 + 1
                                 ]
report fin_Allocation
end


to-report Are-Tasks-Paired? [Task1 Task2]
  let tmp FALSE
  if (item 0 Task1 = item 0 Task2) AND (item 1 Task1 = item 1 Task2) [set tmp TRUE]
  report tmp
end
   


; this reporter breeds new candidates to be pitted against each other in a new round of the GA
; it performs crossover, copying and finally mutation (at a low rate) of all bits
to-report Create-Next-Generation-Candidates [OldGeneration OldFitness]
  let Tmp_Candidates  []
 
 ;CROSSOVER the first set of new candidates will be created by CROSSOVER of older bit strings (checked)
  let crossover-count  (floor (NUMBER_OF_CANDIDATES * CROSSOVER-RATE / 100 / 2))  
  repeat crossover-count
                [
    ; This uses "tournament selection", with tournament size = 3.
    ; This means, the algorithm randomly pick 3 solutions from the previous generation
    ; and select the best one of those 3 to reProd_Settingsuce.
    ; An equivalent algorithms is used in "Simple Genetic Algorithm" in the NetLogo 5RC4 library
    ; Copyright 2008 Uri Wilensky.

                  let subset n-values 6 [random NUMBER_OF_CANDIDATES] ; select 6 candidates for the turnament
                  
                  let tmp_pos1 item 0 subset
                  if item (item 1 subset) OldFitness > item tmp_pos1 OldFitness [set tmp_pos1 item 1 subset]
                  if item (item 2 subset) OldFitness > item tmp_pos1 OldFitness [set tmp_pos1 item 2 subset]   
                  ; compare the second three and select the one with highest fitness
                  let tmp_pos2 item 3 subset
                  if item (item 4 subset) OldFitness > item tmp_pos2 OldFitness [set tmp_pos2 item 4 subset]
                  if item (item 5 subset) OldFitness > item tmp_pos2 OldFitness [set tmp_pos2 item 5 subset]   
 
                  let child-bits crossover (item tmp_pos1 OldGeneration) (item tmp_pos2 OldGeneration)
                  ; create the two children, with their new genetic material
                  set Tmp_Candidates lput item 0 child-bits Tmp_Candidates
                  set Tmp_Candidates lput item 1 child-bits Tmp_Candidates
                ]

;REProd_SettingsUCTION - the second set of candidates will "simply" be COPIED, but again selecting for fitness (checked)
  repeat (NUMBER_OF_CANDIDATES - crossover-count * 2)
                [
                  let subset n-values 3 [random NUMBER_OF_CANDIDATES] ; select 6 candidates for the turnament
                  let tmp_pos1 item 0 subset
                  if item (item 1 subset) OldFitness > item tmp_pos1 OldFitness [set tmp_pos1 item 1 subset]
                  if item (item 2 subset) OldFitness > item tmp_pos1 OldFitness [set tmp_pos1 item 2 subset] 
       
                  set Tmp_Candidates lput item tmp_pos1 OldGeneration Tmp_Candidates
                ]
; if prints [print (word "After copying candidates are: " Tmp_Candidates)]

; MUTATION - lastly mutate randomly all the new candidates. This means that every bit faces a chance of 
; swapping its value of "MUTATION-RATE" percent. (CHECKED)
  
  let Final_Candidates []
  foreach Tmp_Candidates [
                          let tmp_can ?
                          let Mutation (map [ifelse-value (random-float 100 < MUTATION-RATE) [1 - ?] [?]] tmp_can)
                         ; print (word "start with " tmp_can )
                         ; print (word "mutated to " Mutation)
                          set Final_Candidates lput Mutation Final_Candidates
                          ]

  report Final_Candidates
end



; turtle reporter procedure
; takes the combined contracts of two turtles and combines them to one list
to-report Merge-Two-Agents-Negotiable-Contracts [AgentA AgentB]
  let ContractsA [Contracts_Held] of AgentA
  let ContractsB [Contracts_Held] of AgentB
  report sentence ContractsA ContractsB
end



; agent procedure
; from a list of contract triplets held, this function calculates the total volume of orders for each of the goods
; it returns a list with essentially a sum over all goods
to-report Extract-Prod_Settingsuction-From-Contracts [ListOfContracts]
   let tmp_Prod_Settingsuction n-values No_Goods [0]                ;[ 0 0 0 0 0 0 0 0 ]
   if NOT empty? ListOfContracts 
          [
           foreach ListOfContracts
                     [
                       let tmpGood   item 0 ?
                       let tmpUnits  item 1 ? ;dont need to Prod_Settingsuce these anymore...
                       
                       set tmp_Prod_Settingsuction replace-item tmpGood tmp_Prod_Settingsuction ((item tmpGood tmp_Prod_Settingsuction) + tmpUnits)
                       ; for each contract, go to tmp_Prod_Settingsuction and add the volume of the contract to the indicated item on the list.
                       ]
            ]
   report tmp_Prod_Settingsuction
 end

;reporter that merges two lists of contracts to one list of units that need to be Prod_Settingsuced (checked)
to-report Calculate-Prod_Settingsuction-For-Agent [CCommitted CHeld]
  let tmp_Held Extract-Prod_Settingsuction-From-Contracts CHeld
  let tmp_All []
  ifelse empty? CCommitted 
                  [
                    set tmp_All tmp_Held
                  ]
                  [
                    let tmp_Committed Extract-Prod_Settingsuction-From-Contracts CCommitted
                    set tmp_All (map [?1 + ?2] tmp_Held tmp_Committed)
                  ]
  report tmp_All
end

; agent reporter, disassembles info in contracts, has total volumes calculated 
; and then solves for a list of time required to Prod_Settingsuce said volumes per good
to-report Calculate-Prod_Settingsuction-Time [CCommitted CHeld Coefficients Exponents]
  let tmpProd_Settingsuction Calculate-Prod_Settingsuction-For-Agent CCommitted CHeld          ; sumarises the total volume of Prod_Settingsuction over all the contracts currently held by the agent
  let Time_Allocation (map [ ( ?1 / ?2 ) ^ ( 1 / ?3 ) ] tmpProd_Settingsuction Coefficients Exponents)
  report Time_Allocation
end

; subtracts the sum of all the time necessary to Prod_Settingsuce ContractsHeld from the total Workhours per day
; returns a number
to-report Calculate_Leisure [CCommitted CHeld Coefficients Exponents]
  report Workhours - sum Calculate-Prod_Settingsuction-Time CCommitted CHeld Coefficients Exponents
end





;###########################################################################################
;############# GA Sub Procedures ###########################################################
;###########################################################################################
 


;Checked
;reporter, creates CNumber random bit strings of length CLength
to-report Create-Candidates [CNumber CLength]
    let Tmp_Candidates  []
        ;insert number of candidate bit-strings here 
        repeat CNumber ; 0 means agentA takes over task, 1 means agentB takes over
               [
                let one_Candidate n-values CLength [one-of [0 1]]
                set Tmp_Candidates lput one_Candidate Tmp_Candidates
               ]
report Tmp_Candidates
end


to-report Calculate-Fitness-All-Candidates [Set-Of-Contracts Candidates AgentA AgentB]
  ; calculate the time that both agents currently have to invest in Prod_Settingsuction (givent their contracts held, and contracts committed)
  let Init-Time-A 0
  ask AgentA [
              let Init_Prod_Settings Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts_Held
              set Init-Time-A Estimate_Prod_Settingsuction_Time_Set Init_Prod_Settings Prod_Settingsuction_Memory
              ]
  let Init-Time-B 0
  ask AgentB [
              let Init_Prod_Settings Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts_Held
              set Init-Time-B Estimate_Prod_Settingsuction_Time_Set Init_Prod_Settings Prod_Settingsuction_Memory
              ] 


  let List-Of-Fitnesses []
  ;calculate the actual fitness score; this requires - the original ppoled set of contracts; the candidate bit string; 
  ; and estimates for the current time spent for both agents, as well as the agents
  foreach Candidates [ set List-Of-Fitnesses lput (Calculate-Fitness-Single-Allocation Set-Of-Contracts ? Init-Time-A Init-Time-B AgentA AgentB) List-Of-Fitnesses]
  
  report List-Of-Fitnesses
end


; calculates the fitness for any Bitstring of Allocations that is as long as the Set-Of-Contracts
to-report Calculate-Fitness-Single-Allocation [Set-Of-Contracts Allocation Init-Time-A Init-Time-B AgentA AgentB]

  ; decode the allocation bitstring into "this task goes to A, this one goes to B..."
  let Contracts-ForA []
  let Contracts-ForB []
  let tmp 0
  foreach Allocation [ifelse ? = 0 
                        [set Contracts-ForA lput (item tmp Set-Of-Contracts) Contracts-ForA]
                        [set Contracts-ForB lput (item tmp Set-Of-Contracts) Contracts-ForB]
                       set tmp tmp + 1 
                     ]
 
  let New-Time-A 0
  ask AgentA [
              ;derive an ordered list that indicates which tasks AgentA has to perform (combining committed and newly allocated tasks)
              let New_Prod_Settings-A Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts-ForA
              ;Estimate the time for that particular combination of tasks
              set New-Time-A Estimate_Prod_Settingsuction_Time_Set New_Prod_Settings-A Prod_Settingsuction_Memory
              ]
  
  let New-Time-B 0
  ask AgentB [
              let New_Prod_Settings-B Calculate-Prod_Settingsuction-For-Agent Contracts_Committed Contracts-ForB
              set New-Time-B Estimate_Prod_Settingsuction_Time_Set New_Prod_Settings-B Prod_Settingsuction_Memory
              ] 
  
  ; the actually time saved
  let Time-Saved-A (Init-Time-A - New-Time-A)
  let Time-Saved-B (Init-Time-B - New-Time-B)
  
  ;penalty for loosing time
  let Penalty 0
  if Time-Saved-A < 0 [set Penalty Penalty + Time-Saved-A ]
  if Time-Saved-B < 0 [set Penalty Penalty + Time-Saved-B]
  
  ; the fitness function - the more time saved, the better. Fairness is discounted and there is an extra pennalty  for actually loosing time
  let tmp_Fitness  (Time-Saved-A + Time-Saved-B) - ((Time-Saved-A - Time-Saved-B) ^ 2) + Penalty
  report tmp_Fitness 
end



;; ===== Mutations
; from Wilensky's "Simple Genetic Algorithm"
;; This reporter performs one-point crossover on two lists of bits.
;; That is, it chooses a random location for a splitting point.
;; Then it reports two new lists, using that splitting point,
;; by combining the first part of bits1 with the second part of bits2
;; and the first part of bits2 with the second part of bits1;
;; it puts together the first part of one list with the second part of
;; the other.
to-report Crossover [bits1 bits2]
  let split-point 1 + random (length bits1 - 1)
  report list (sentence (sublist bits1 0 split-point)
                        (sublist bits2 split-point length bits2))
              (sentence (sublist bits2 0 split-point)
                        (sublist bits1 split-point length bits1))
end



;###########################################################################################
;############# NSearches Procedures ########################################################
;###########################################################################################
 

;Leisure_Memory             ; list of length Mem_Length storing the agent's most recently achieved levels of leisure
;Search_Prob_Vector         ; Vector of Probabilities to undertake n searches for partners, innitiated as geometric distribution with parameter GPROB
;Round_Searches             ; every round, the agent will update this variable by choosing a number of searches from the 'Search_Prob_Vector'
;Round_Searches_left        ; indicates the number of searches still available to the agent in any given round

; this creates a vector of length No_Agents filled with the probabilities of a geometric distribution with parameter p=the_GPROB  (checked)
to-report Initiate_Search_Prob_Vector [the_GPROB]
  let tmp []
  let counter 0
 ; while [counter < No_Agents] too restrictive in small societies
  while [counter < 10 ]
           [ set tmp lput  ( the_GPROB * (1 - the_GPROB) ^ counter ) tmp
             set counter counter + 1
             ]
report tmp
end

; update the Search_Prob_Vector - if this round was successful, increase the probability of searching that often, otherwise, decrease it (check)
; run this after calculation the current leisure and before updating the LeisureMemory vector
to-report Update_Search_Prob_Vector [Vector cLeisure vLeisure_Memory cRound_Searches]
  let tmpVec Vector
  ifelse cLeisure < mean vLeisure_Memory
      [ ; if the experience was below previous standards - reduce prob by increment
        set tmpVec replace-item cRound_Searches tmpVec (item cRound_Searches tmpVec - Search_Prob_Increment)
        if item cRound_Searches tmpVec < 0 [ set tmpVec replace-item cRound_Searches tmpVec 0]
        ]
      [ ; if the experience was at least equal to the previous experiences - increase prob by increment
        set tmpVec replace-item cRound_Searches tmpVec (item cRound_Searches tmpVec + Search_Prob_Increment) 
        ] 
 
  set tmpVec map [? / (sum tmpVec)] tmpVec  
  report tmpVec
end
  

; puts the latest realised Leisure value at the beginning of the agent's memory of leisure memories (checked)
to-report Update_Leisure_Memory [tmp_Leisure_Memory tmp_Leisure]
  let tmp fput tmp_Leisure (but-last tmp_Leisure_Memory)
  report tmp
end


;with probabilitiess specified in Vector ( Search_Prob_Vector ), this procedure chooses a number of searches to be performed in this round (checked)
to-report Decide_NSearches [ Vector ]
  let Target_Score random-float (sum Vector)
  let tmp_Score 0
  let tmpN 0
;  let tmp_Partner nobody
  if Target_Score > 0
     [
       while [tmp_Score <= Target_Score ]
            [
             set  tmp_Score ( tmp_Score + ( item tmpN Vector ) )
             set  tmpN tmpN + 1
            ]
       ]
  set tmpN tmpN - 1   
  ; add or subtract 'errors', i.e. introduce some variation, but keep within limits [0, No_Agents=length Vector]
  if random 100 < PError_NSearch [ 
                                   ifelse tmpN = 0 [ set tmpN 1 ] 
                                                   [ ifelse random-float 1 < 0.5 
                                                        [set tmpN tmpN - 1]
                                                        [set tmpN tmpN + 1]
                                                   ] 
                                   if tmpN > (length Vector - 1) [ set tmpN (length Vector - 1) ]                    
                                      ]
  report tmpN
end


; ############# Wrapper for Prod_Settingsuction Time Estimates

; has inputs a) a list of the numbers of units per good - and b) the agent's entire memory regarding the Prod_Settingsuction
to-report Estimate_Prod_Settingsuction_Time_Set [MyProd_Settingsuction Prod_SettingsuctionMemory]
  let Estimates n-values No_Goods [0]  ;initiate an empty list
  let tmp 0  ;index for Goods
  foreach MyProd_Settingsuction
           [
             if ? > 0
                      [
                       let Estimate Estimate_Prod_Settingsuction_Time_Single tmp ? Prod_SettingsuctionMemory ;that is Good Units Memory
                       set Estimates replace-item tmp Estimates Estimate
                      ]
              set tmp tmp + 1
             ]
  report sum Estimates
end


;####### Estimate of Prod_Settingsuction Time

to-report Estimate_Prod_Settingsuction_Time_Single [TmpGood TmpUnits Memory]
 
 let RememberedUnits item TmpGood (item 0 Memory) ;retrieve memory for that particular Good (Units)
 let RememberedTimes item TmpGood (item 1 Memory)  ;(Time) 
 
 let found? position TmpUnits RememberedUnits
 let Estimate []
 
 ifelse is-number? found? 
       [ 
         set Estimate item found? RememberedTimes      
         ]
       [ ifelse max RememberedUnits < TmpUnits ; check if the proposed amount exceeds what has been Prod_Settingsuced before
                   [ ;extrapolate linearly from highest memory available
                     let maxUnitsRemembered max RememberedUnits
                     set Estimate (max RememberedTimes) * TmpUnits / maxUnitsRemembered ;assuming that origin of Prod_Settingsuction function is at the origin  
                     ; should always overestimate the time-costs     
                   ]
                   [ ;regress linearly between two remembered units of Prod_Settingsuction
                     let positionfinder 0
                     foreach RememberedUnits [if ? < TmpUnits [set Positionfinder Positionfinder + 1]]
                     
                     let x1 item (positionfinder - 1) RememberedUnits
                     let x2 item  positionfinder      RememberedUnits
                     let y1 item (positionfinder - 1) RememberedTimes
                     let y2 item  positionfinder      RememberedTimes
                     
                     let b (y2 - y1) / (x2 - x1)
                     let a y1 - (b * x1)
                     
                     set Estimate a + (b * TmpUnits)  
                     ; should always !!underestimate!! the time-costs (slightly)
                     ]
        ]
 report Estimate
end


;########### Prod_Settingsuction Memories

to-report Initiate_Prod_Settingsuction_Memories [ContractsHeld MyCoefficients MyExponents]
  let Units n-values No_Goods [1]                                                          ;[1 1 1 1 1 1]  ;ASSUMPTION, everyone knows 
                                                                                           ; how much time they need to Prod_Settingsuce one unit of any good
  let Time (map [ ( ?1 / ?2 ) ^ ( 1 / ?3 ) ] Units MyCoefficients MyExponents)             ;[list of inverse coefficients] ;if exponentts = 1
  
  let ListOfUnits []
  let ListOfTime  []
  
  let tmp 0
     foreach Time [
                   set ListOfUnits lput (list 1) ListOfUnits                             ;[[1] [1] [1] [1] [1]]
                   set ListOfTime lput (list ?)  ListOfTime                              ;[[1] [.333] [2] [0.5] [1]]    (see above) 
     ]
  let ListOfMemories (list ListOfUnits ListOfTime)                                       ;[   [[1] [1] [1] [1] [1]]    [[1] [.333] [2] [0.5] [1]]   ]
  
  set ListOfMemories Update_Prod_Settingsuction_Memories ListOfMemories ContractsHeld [] MyCoefficients MyExponents
  report ListOfMemories 
end



to-report Update_Prod_Settingsuction_Memories [Memories ContractsHeld ContractsCommitted MyCoefficients MyExponents]
  let Units Calculate-Prod_Settingsuction-For-Agent ContractsHeld ContractsCommitted               ; get units currently Prod_Settingsuced
  let Time  Calculate-Prod_Settingsuction-Time      ContractsHeld ContractsCommitted  MyCoefficients MyExponents            ; calculate vector of time needed
  ;these are lists of length No_Goods
  
  let RememberedUnits item 0 Memories                                                      ; retrieve memories of units
  let RememberedTimes item 1 Memories                                                      ; retrieve memories of time
  ;these are matrix-like lists No_Goods x #Memories 
  
  let tmpGood 0
  ; let this counter move through the indices for goods
  foreach Units [ if ? > 0 [ let RememberedUnitsForGood item tmpGood RememberedUnits       ; vector of units of that good Prod_Settingsuced so far
                             let RememberedTimesForGood item tmpGood RememberedTimes       ; vector of time needed to Prod_Settingsuce these units
                             
                             let MaxUnitsMemory max RememberedUnitsForGood                 ; temp for the maximum of units Prod_Settingsuced so far - say 5
                             
                             ifelse MaxUnitsMemory < ?                                     ; is "5" is below ? (the current units)
                                  [ ;if the new amount is higher than all previous amounts - just append units and time to the respective lists
                                    set RememberedUnitsForGood sentence RememberedUnitsForGood ?                 ;the current units (new max)
                                    set RememberedTimesForGood sentence RememberedTimesForGood item tmpGood Time ;the current time
                                    ]
                                  [ ;replace (if ? is in list already) - or squeeze in
                                    let InList? position ? RememberedUnitsForGood ;search for the position of ? in current memory
                                    
                                    ifelse is-number? InList? ; if that position is defined, the items of that position are replaced
                                        [ 
                                          ; if a prior entry for that amount is found in memory, it is updated
                                          set RememberedUnitsForGood replace-item InList? RememberedUnitsForGood ?
                                          set RememberedTimesForGood replace-item InList? RememberedTimesForGood item tmpGood Time  
                                          ]
                                        [ ;position can be FALSE or number
                                          ; as long as Prod_Settingsuction functions are monotonic, we can just append the new piece and sort both lists ascendingly
                                          set RememberedUnitsForGood sentence RememberedUnitsForGood ?                       ;put these two together
                                          set RememberedTimesForGood sentence RememberedTimesForGood item tmpGood Time       ;same for time
                                          set RememberedUnitsForGood sort RememberedUnitsForGood ;this may become difficult with non-monotonic Prod_Settingsuction functions
                                          set RememberedTimesForGood sort RememberedTimesForGood 
                                          ]                                     
                                    ]
                              ; the memory entries for tmpGood  are replaced
                              set RememberedUnits replace-item tmpGood RememberedUnits RememberedUnitsForGood
                              set RememberedTimes replace-item tmpGood RememberedTimes RememberedTimesForGood    
                             ] 
                  set tmpGood tmpGood + 1
                  ]                 

  report (list RememberedUnits RememberedTimes)
end
@#$#@#$#@
GRAPHICS-WINDOW
10
215
284
510
16
16
8.0
1
10
1
1
1
0
1
1
1
-16
16
-16
16
1
1
1
ticks
30.0

BUTTON
80
10
143
43
NIL
Go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
10
10
79
43
NIL
Setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
145
10
208
43
NIL
Go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
10
75
182
108
No_Agents
No_Agents
0
20
12
1
1
NIL
HORIZONTAL

SLIDER
10
115
182
148
No_Goods
No_Goods
0
10
2
1
1
NIL
HORIZONTAL

SLIDER
295
295
467
328
Loyalty
Loyalty
0
100
99
1
1
NIL
HORIZONTAL

SLIDER
295
375
472
408
Search_Prob_Increment
Search_Prob_Increment
0
0.5
0.1
0.1
1
NIL
HORIZONTAL

PLOT
500
15
700
165
Average Leisure
NIL
NIL
0.0
10.0
-2.0
4.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot mean [Leisure] of Turtles"

SLIDER
295
335
467
368
Mem_Length
Mem_Length
0
10
8
1
1
NIL
HORIZONTAL

SWITCH
295
215
457
248
Search_Random
Search_Random
1
1
-1000

SLIDER
295
255
467
288
Search_Cost
Search_Cost
0
0.4
0.15
0.05
1
NIL
HORIZONTAL

SLIDER
295
415
467
448
PError_NSearch
PError_NSearch
0
100
25
1
1
NIL
HORIZONTAL

TEXTBOX
305
165
455
206
Settings affecting Partner Search and Relationship Development
11
0.0
1

TEXTBOX
15
55
165
73
General Paramters
11
0.0
1

SLIDER
10
160
182
193
GA-Rounds
GA-Rounds
0
50
20
1
1
NIL
HORIZONTAL

CHOOSER
200
105
338
150
Prod_Settings
Prod_Settings
"CSW" "4G" "4Gh" "none"
0

PLOT
715
175
915
325
Agent Degree
NIL
NIL
0.0
6.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "histogram DegreeVector"

PLOT
500
175
700
325
Agent Specialisation
NIL
NIL
50.0
100.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -16777216 true "" "histogram map [? * 100] SpecialisationVector"

PLOT
500
340
700
490
Agent Searches
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 1 -13345367 true "" "histogram SearchesVector"

TEXTBOX
205
85
355
103
Preset production settings
11
0.0
1

PLOT
715
15
915
165
Average Exchange
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot mean ExchangeVolumeVector"

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
0
Rectangle -7500403 true true 151 225 180 285
Rectangle -7500403 true true 47 225 75 285
Rectangle -7500403 true true 15 75 210 225
Circle -7500403 true true 135 75 150
Circle -16777216 true false 165 76 116

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.0.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="CSW" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="100"/>
    <metric>The_Seed</metric>
    <metric>SpecialisationVector</metric>
    <metric>EfficiencyVector</metric>
    <metric>LeisureVector</metric>
    <metric>DegreeVector</metric>
    <metric>SearchesVector</metric>
    <metric>ExchangeVolumeVector</metric>
    <enumeratedValueSet variable="No_Agents">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Random">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Mem_Length">
      <value value="2"/>
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Rejection">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GA-ROUNDS">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="No_Goods">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Cost">
      <value value="0"/>
      <value value="0.1"/>
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Prob_Increment">
      <value value="0.1"/>
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Loyalty">
      <value value="50"/>
      <value value="75"/>
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GPROB">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PError_NSearch">
      <value value="5"/>
      <value value="25"/>
      <value value="50"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="CSW_serious" repetitions="30" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="75"/>
    <metric>The_Seed</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean ExchangeVolumeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation ExchangeVolumeVector]</metric>
    <enumeratedValueSet variable="No_Agents">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Random">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Mem_Length">
      <value value="2"/>
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Rejection">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GA-ROUNDS">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="No_Goods">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Cost">
      <value value="0"/>
      <value value="0.2"/>
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Prob_Increment">
      <value value="0.1"/>
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Loyalty">
      <value value="50"/>
      <value value="75"/>
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GPROB">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PError_NSearch">
      <value value="5"/>
      <value value="25"/>
      <value value="50"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="CSW_serious_random" repetitions="30" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="75"/>
    <metric>The_Seed</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean ExchangeVolumeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation ExchangeVolumeVector]</metric>
    <enumeratedValueSet variable="No_Agents">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Random">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Mem_Length">
      <value value="2"/>
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Rejection">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GA-ROUNDS">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="No_Goods">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Cost">
      <value value="0"/>
      <value value="0.2"/>
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Prob_Increment">
      <value value="0.1"/>
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Loyalty">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GPROB">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PError_NSearch">
      <value value="5"/>
      <value value="25"/>
      <value value="50"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="CSW_serious_GALoyalty" repetitions="30" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="75"/>
    <metric>The_Seed</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean ExchangeVolumeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation ExchangeVolumeVector]</metric>
    <enumeratedValueSet variable="No_Agents">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Random">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Mem_Length">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Rejection">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GA-ROUNDS">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="No_Goods">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Cost">
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Prob_Increment">
      <value value="0.1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="Loyalty" first="75" step="2" last="99"/>
    <enumeratedValueSet variable="GPROB">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PError_NSearch">
      <value value="25"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="CSW_serious_SCLoyalty" repetitions="30" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="75"/>
    <metric>The_Seed</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean ExchangeVolumeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation ExchangeVolumeVector]</metric>
    <enumeratedValueSet variable="No_Agents">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Random">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Mem_Length">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Rejection">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GA-ROUNDS">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="No_Goods">
      <value value="2"/>
    </enumeratedValueSet>
    <steppedValueSet variable="Search_Cost" first="0" step="0.5" last="3"/>
    <enumeratedValueSet variable="Search_Prob_Increment">
      <value value="0.1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="Loyalty" first="75" step="2" last="99"/>
    <enumeratedValueSet variable="GPROB">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PError_NSearch">
      <value value="25"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="4G Serious" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <timeLimit steps="75"/>
    <metric>The_Seed</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SpecialisationVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation LeisureVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation DegreeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation SearchesVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [mean ExchangeVolumeVector]</metric>
    <metric>ifelse-value (ticks = 0) [0] [standard-deviation ExchangeVolumeVector]</metric>
    <enumeratedValueSet variable="No_Agents">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Random">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Mem_Length">
      <value value="2"/>
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Rejection">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GA-ROUNDS">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
      <value value="30"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="No_Goods">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Cost">
      <value value="0"/>
      <value value="0.2"/>
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Search_Prob_Increment">
      <value value="0.1"/>
      <value value="0.4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Loyalty">
      <value value="50"/>
      <value value="75"/>
      <value value="99"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="GPROB">
      <value value="0.7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="PError_NSearch">
      <value value="5"/>
      <value value="25"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="Prod">
      <value value="&quot;heteroP&quot;"/>
      <value value="&quot;homoP&quot;"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 1.0 0.0
0.0 1 1.0 0.0
0.2 0 1.0 0.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
1
@#$#@#$#@
